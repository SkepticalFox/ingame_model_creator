from Avatar import PlayerAvatar
from gui import InputHandler
import BigWorld, game

origin_onEnterWorld = PlayerAvatar.onEnterWorld
def new_onEnterWorld(self, prereqs):
    origin_onEnterWorld(self, prereqs)
    InputHandler.g_instance.onKeyDown += inject_handle_key_event
PlayerAvatar.onEnterWorld = new_onEnterWorld

def inject_handle_key_event(event):
    # import Keys
    if event.key == 25: # Keys.KEY_P
        put_model()

origin_onLeaveWorld = PlayerAvatar.onLeaveWorld
def new_onLeaveWorld(self):
    origin_onLeaveWorld(self)
    InputHandler.g_instance.onKeyDown -= inject_handle_key_event
PlayerAvatar.onLeaveWorld = new_onLeaveWorld

def put_model():
    print '[put_model]'
    player = BigWorld.player()
